﻿using Newtonsoft.Json.Linq;
using System.IO;
using System.Net;
using System.Text;

namespace LiveHouse
{
    class GetAPIData
    {
        public JArray GetResponse(string apiUrl, string jsonParameter)
        {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);
                request.Method = "GET";
                request.ContentType = "application/json;";
                // カスタムヘッダーが必要の場合(認証トークンとか)

                // request.Headers.Add("custom-api-param", "value");
                HttpWebResponse webres = (HttpWebResponse)request.GetResponse();

                //応答データを受信するためのStreamを取得
                Stream st = webres.GetResponseStream();
                //文字コードを指定して、StreamReaderを作成
                StreamReader sr = new StreamReader(st, Encoding.UTF8);
                //データをすべて受信
                var response = JArray.Parse(sr.ReadToEnd());

                //閉じる
                sr.Close();

                return response;

        }
    }
}
