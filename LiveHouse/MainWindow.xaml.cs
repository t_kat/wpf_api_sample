﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Web.Script.Serialization;
using System.Windows;


namespace LiveHouse
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Livehouses> livehouses = new List<Livehouses>();
        private List<Players> players = new List<Players>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void SHOW(object sender, RoutedEventArgs e)
        {
            var jsonParameter = new JavaScriptSerializer().Serialize(new
            {
                address = "address",
                name = "name"
            });

            GetAPIData api = new GetAPIData();
            var a = api.GetResponse($"http://153.126.211.236/livehouses", jsonParameter);
            var count = 1;

            foreach (var item in a)
            {
                JValue nameValue = (JValue)item["name"];
                string name = (string)nameValue.Value;

                JValue addressValue = (JValue)item["address"];
                string address = (string)addressValue.Value;

                livehouses.Add(new Livehouses() { number = count++, name = name, address = address });

            }

            var data = new ObservableCollection<Livehouses>(livehouses);

            // DataGridに設定する
            this.APIData.ItemsSource = data;
        }

        private void SHOW2(object sender, RoutedEventArgs e)
        {
            var jsonParameter = new JavaScriptSerializer().Serialize(new
            {
                name = "name",
                instrument = "instrument"
            });

            GetAPIData api = new GetAPIData();
            var a = api.GetResponse($"http://153.126.211.236/players", jsonParameter);
            var count = 1;

            foreach (var item in a)
            {
                JValue nameValue = (JValue)item["name"];
                string name = (string)nameValue.Value;

                JValue instrumentValue = (JValue)item["instrument"];
                string instrument = (string)instrumentValue.Value;

                players.Add(new Players() { number = count++, name = name, instrument = instrument });

            }

            var data = new ObservableCollection<Players>(players);

            // DataGridに設定する
            this.APIData.ItemsSource = data;
        }
    }
}
